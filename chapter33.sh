#!/usr/bin/env bash
########################
#owner: droritzz
#purpose: homework lnxessentials
#date: 01/11/20
#version: in git commit
#######################

#set up a directory, owned by the group sports.
student@vaiolabs:~/homework/lnxessentials$ groupadd sports
groupadd: group 'sports' already exists
student@vaiolabs:~/homework/lnxessentials$ mkdir /home/sports
mkdir: cannot create directory ‘/home/sports’: Permission denied
student@vaiolabs:~/homework/lnxessentials$ su -
Password:
root@vaiolabs:~# mkdir /home/sports
root@vaiolabs:~# man chown
root@vaiolabs:~# chown root:sports /home/sports
root@vaiolabs:~# chown root:sports /home/sports
root@vaiolabs:~# cd /home
root@vaiolabs:/home# ls -la
total 16
drwxr-xr-x  4 root    root    4096 Nov  1 18:01 .
drwxr-xr-x 22 root    root    4096 Oct  2 01:43 ..
drwxr-xr-x  2 root    sports  4096 Nov  1 18:01 sports
drwxr-xr-x 15 student student 4096 Nov  1 13:55 student


#members of the sports group should be able to create files in this directory
root@vaiolabs:/home# chmod g+w /home/sports
root@vaiolabs:/home# ls -la
total 16
drwxr-xr-x  4 root    root    4096 Nov  1 18:01 .
drwxr-xr-x 22 root    root    4096 Oct  2 01:43 ..
drwxrwxr-x  2 root    sports  4096 Nov  1 18:01 sports
drwxr-xr-x 15 student student 4096 Nov  1 13:55 student


#all files created in this directory should be group-owned by the sports group
root@vaiolabs:/home# chmod g+s /home/sports
root@vaiolabs:/home# ls -la
total 16
drwxr-xr-x  4 root    root    4096 Nov  1 18:01 .
drwxr-xr-x 22 root    root    4096 Oct  2 01:43 ..
drwxrwsr-x  2 root    sports  4096 Nov  1 18:01 sports
drwxr-xr-x 15 student student 4096 Nov  1 13:55 student

#users should be able to delete only their own user-owned files

root@vaiolabs:/home# chmod +t /home/sports
root@vaiolabs:/home# ls -la
total 16
drwxr-xr-x  4 root    root    4096 Nov  1 18:01 .
drwxr-xr-x 22 root    root    4096 Oct  2 01:43 ..
drwxrwsr-t  2 root    sports  4096 Nov  1 18:01 sports
drwxr-xr-x 15 student student 4096 Nov  1 13:55 student

#test that this works!

root@vaiolabs:/home# useradd -g sports -c "Serena Williams" serena
root@vaiolabs:/home# su serena
$ id
uid=1001(serena) gid=1003(sports) groups=1003(sports)
$ cd /home/sports
$ touch file_serena
$ ls -ls
total 0
0 -rw-r--r-- 1 serena sports 0 Nov  1 18:27 file_serena
$ su student
Password:
student@vaiolabs:/home/sports$ pwd
/home/sports
student@vaiolabs:/home/sports$ touch file_student
touch: cannot touch 'file_student': Permission denied

#veify permissions on /usr/bin/passwd . remove the setuid, then try changing your password as a normal user. reset the permissions back and try again.
