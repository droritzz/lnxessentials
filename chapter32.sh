#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################
 

#as normal user create a directory ~/persmissions. create a file owned by yourself in there

student@localhost:~> mkdir ~/permessions
student@localhost:~> cd ~/permessions/
student@localhost:~/permessions> touch myfile
student@localhost:~/permessions> ls -la
total 0
drwxr-xr-x 1 student users  12 Oct 28 21:53 .
drwxr-xr-x 1 student users 704 Oct 28 21:53 ..
-rw-r--r-- 1 student users   0 Oct 28 21:53 myfile


#copy a file owned by root from /etc/ to your permissions dir, who owns this file now?

student@localhost:/etc> cp bash.bashrc ~/permessions/copy_root_from_etc
tudent@localhost:~/permessions> ls -la
total 12
drwxr-xr-x 1 student users    48 Oct 28 21:56 .
drwxr-xr-x 1 student users   704 Oct 28 21:53 ..
-rw-r--r-- 1 student users 10393 Oct 28 21:56 copy_root_from_etc
-rw-r--r-- 1 student users     0 Oct 28 21:53 myfile

#####student user does

#as root, create a file in the users ~/permissions dir

student@localhost:~/permessions> sudo touch root_file
[sudo] password for root: 
student@localhost:~/permessions> ls -la
total 12
drwxr-xr-x 1 student users    66 Oct 28 22:04 .
drwxr-xr-x 1 student users   704 Oct 28 21:53 ..
-rw-r--r-- 1 student users 10393 Oct 28 21:56 copy_root_from_etc
-rw-r--r-- 1 student users     0 Oct 28 21:53 myfile
-rw-r--r-- 1 root    root      0 Oct 28 22:04 root_file

#as normal user, look at who owns this file created by root

####root does

#change the ownership of all files in ~/permissions to yourself

student@localhost:~/permessions> chown student *
chown: changing ownership of 'root_file': Operation not permitted
student@localhost:~/permessions> chown student root_file
chown: changing ownership of 'root_file': Operation not permitted
student@localhost:~/permessions> chown student ~/permessions/*
chown: changing ownership of '/home/student/permessions/root_file': Operation not permitted
#NO CAN DO....

#make sure you have all rights to these files and others can only read

student@localhost:~/permessions> chmod 744 myfile 
student@localhost:~/permessions> chmod 744 copy_root_from_etc 
student@localhost:~/permessions> ls -la
total 12
drwxr-xr-x 1 student users    66 Oct 28 22:04 .
drwxr-xr-x 1 student users   704 Oct 28 21:53 ..
-rwxr--r-- 1 student users 10393 Oct 28 21:56 copy_root_from_etc
-rwxr--r-- 1 student users     0 Oct 28 21:53 myfile
-rw-r--r-- 1 root    root      0 Oct 28 22:04 root_file

#with chmod, is 770 the same as rwxrwx---

#yes

#with chmod is 664 the same as r-xr-xr--

#no

#with chmod is 400 the same as r--------

#yes

#with chmod is 734 the same as rwxr-xr--

#no

#display the unmask in octal and in symbolic form

Man: What manual page do you want?
Man: 1p
student@localhost:~/permessions> umask
0022
student@localhost:~/permessions> umask -S
u=rwx,g=rx,o=rx

#set the unmask to 077 but use the symbolic format to set it. verify that this works

student@localhost:~/permessions> umask -S 077
u=rwx,g=,o=
student@localhost:~/permessions> 


#create a file as root, give only read to others. can a normal user read this file? test writing to this file with vi

localhost:/home # touch some_root_file
localhost:/home # ls -la
total 0
drwxr-xr-x 1 root    root    64 Oct 28 22:35 .
drwxr-xr-x 1 root    root   156 Sep  2 22:05 ..
drwxr-xr-x 1    1002 tennis 244 Oct 24 14:40 serena
-rw-r--r-- 1 root    root     0 Oct 28 22:35 some_root_file
drwxr-xr-x 1 student users  704 Oct 28 21:53 student
drwxr-xr-x 1    1003 users  228 Oct 24 12:50 venus
localhost:/home # chmod 744 some_root_file
localhost:/home # ls -la
total 0
drwxr-xr-x 1 root    root    64 Oct 28 22:35 .
drwxr-xr-x 1 root    root   156 Sep  2 22:05 ..
drwxr-xr-x 1    1002 tennis 244 Oct 24 14:40 serena
-rwxr--r-- 1 root    root     0 Oct 28 22:35 some_root_file
drwxr-xr-x 1 student users  704 Oct 28 21:53 student
drwxr-xr-x 1    1003 users  228 Oct 24 12:50 venus
localhost:/home # exit


#create a file as a normal user, give only read to others. can another normal user read this file? test writing to this file with vi

student@localhost:~> chmod u+rwx, g+r, o+r some_normal_user_file.txt
chmod: invalid mode: ‘u+rwx,’
Try 'chmod --help' for more information.
student@localhost:~> chmod 744
chmod: missing operand after ‘744’
Try 'chmod --help' for more information.
student@localhost:~> chmod 744 some_normal_user_file.txt

-rwxr--r-- 1 student users     0 Oct 28 22:46 some_normal_user_file.txt
 ## everyone can read the file

#can root read this file? can root write to this file with vi?
#### yes, root is a god in this small universe

#creat a dir that belongs to a group, where every member of that group can read and write to files and create files. make sure that people can only delete their own files

dc:x:1003:batman,aquaman,the_flash
localhost:~ # mkdir justice_league
localhost:~ # chgrp dc justice_league/
drwxr-xr-x 1 root dc       0 Oct 28 23:00 justice_league
localhost:~ # chmod 775 justice_league/







