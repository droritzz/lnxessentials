#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################
#make sure you are root, if not begin with su - command


#create a user account named serena, including a home directory and a description that reads Serena Williams, single command

useradd -m -d /home/serena -c "Serena Williams" serena

#create a user named venus, including a home directory, bash shell, a description that reads Venus Williams all in one single command

useradd -m -d /home/venus -c "Venus Williams" -s /bin/bash venus
#verify that both users have correct entries in /etc/passwd /etc/shadow /etc/group

tail -2 /etc/passwd 
tail -2 /etc/shadow
tail -2 /etc/group

#verify that their home directory was created
ls -la /home

#create a user named einstime with /bin/date as his default logon shell
useradd -m -s /bin/date einstime

#logon with einstime user. can you think of a useful real world example for changing a user's login shell to an application?

su einstime
#this user will be yout Mad Hatter and tell you the time and date
#create a file named welcome.txt and make sure every new user will see this file in their home directory
touch /etc/skel/welcome.txt 


#verify this setup by creation and deleting a test user account

useradd -m -d /home/test test
cd /etc/skel && ls

#change the default login shell for the seren user to /bin/bash. verify before and after you make this change.
tail -2 /etc/passwd
usermod -s /bin/bash serena
tail -2 /etc/passwd
