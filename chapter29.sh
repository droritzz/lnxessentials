#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################

#set the password for serena to hunter2

passwd serena
hunter2
hunter2

#also set a password for venus and then lock the venus user account with usermod. verify the locking in /etc/shadow before and after you lock it.
passwd venus
hunter3
hunter3
tail /etc/shadow | grep venus
usermod -L venus
tail /etc/shadow | grep venus

#use passwd -d to disable the serena password. verify the serena line in /etc/shadow before and after disabling
grep serena /etc/shadow 
passwd -d serena 
grep serena /etc/shadow 

#what is the difference between locking a user account and disabling a user account's password, with usermod -L and passwd -d?
# locking the account will lock out all the users, while locking the password will lock the user out, but the root will still be able to access the account.

#try changing the password of serena to serena as serena
passwd serena
Changing password for serena.
Current password: 
New password: 
BAD PASSWORD: it is based on your username
passwd: Authentication token manipulation error
passwd: password unchanged

#make sure serena has to change her password in 10 days

chage -M 10 serena

#make sure every new user needs to change their password every 10 days

########sed -i 's/PASS_MAX_DAYS \*/PASS_MAX_DAYS 10/' /etc/login.defs##############
vi /etc/login.defs


#take a backup as root of /etc/shadow. use vi to copy an encrypted hunter2 hash from venus to serena. can serena now log on with hunter2 as a password?


#why use vipw instead of vi? what is the problem when using vi or vim?
#vipw warns you that this file is open somewhere else. vi opens the file in any case

#use chsh to list all shells (RHEL/CentOS/Fedora) and compare to cat /etc/shells

chsh -s
#user useradd option allows you to name a home direcory?

useradd -d
#how can you see whether the password of user serena is locked or unlocked? give a solution whith grep and a solution with passwd

passwd -S serena
grep serena /etc/shadow

