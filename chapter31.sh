#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 28/10/20
#version: in git commit
#######################


#create the group tennis, football and sports

groupadd tennis
groupadd football
groupadd sports

#in one command make venus a member of tennis and sports

usermod -a -G tennis, sports venus
#rename the football group to foot

groupmod -n football foot

#use vi to add serena to the tennis group

vi /etc/group

#use the id command to verify that serena is a member of tennis

su serena
id

#make someone responsible for managing group membership of foot and sports. test that it works 

gpasswd -A venus sports #management
usermod -a -G foot serena #add to group
gpasswd -A serena foor #management
