#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################

#make a list of all profile files on your system
ls -la /etc/bash.bashrc ; ls -la /*.pro ; ls -la /etc/profile

#read the content of each of these, often they use source extra scripts

#put a unique variable, alias and function in each of those files
vi ~/.bashrc
alias bananas='echo potatos'
#close and reopen the terminal and run bananas command

#try several different way to obtain a shell (su, su -, ssh, tmux, gnome-terminal, ctrl-alt-F1 ) and verify which of your custom variables, aliases and functions are present in your enviroment

#the new alias works only in local enviroment

#do you also know the order in which they are executed?
cat /etc/profile

#when an application depends on a setting in $HOME/.profile, does it matter whether $HOME/.bash_profile exists or not?

#it does matter, if $HOME/.bash_profile exists, app will read from it and will not get to $HOME/.profile
