#!/usr/bin/env bash
########################	
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################

#run a command that displays only your currently logged on user name
whoami

#display a list of all logged on users
users
who

#display a list of all logged on users including the command they are running at this moment
w

#display your user name and your unique user id
id
echo $EUID

#use su to switch to another user account and get back to the previous account
su root

#use su - to switch to another user and notice the difference
su - root #using the su - to switch to another user brings me to user's home directory

#try to create a new user account (when using your normal user account), this would fail. 
useradd droritzz

#try the same, but with sudo before your command
sudo useradd droritzz

