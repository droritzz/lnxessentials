#!/usr/bin/env bash
########################
#owner: droritzz
#purpose: homework lnxessentials
#date: 23/10/20
#version: in git commit
#######################

#rcreate two files named winter.txt and summer.txt, put some text in them
student@vaiolabs:~$ touch winter.txt summer.txt
student@vaiolabs:~$ ls
Desktop  Downloads  homework  Projects  summer.txt  winter.txt

#creat a hard link to winter.txt named hlwinter.text
student@vaiolabs:~$ ln winter.txt hlwinter.txt
student@vaiolabs:~$ ls -li
total 16
1059211 drwxr-xr-x 2 student student 4096 Sep  7 13:11 Desktop
1059212 drwxr-xr-x 2 student student 4096 Oct  2 01:44 Downloads
1063817 -rw-r--r-- 2 student student    0 Nov  2 15:56 hlwinter.txt
1059225 drwxr-xr-x 3 student student 4096 Nov  1 13:52 homework
1059213 drwxr-xr-x 3 student student 4096 Sep  7 13:12 Projects
1063922 lrwxrwxrwx 1 student student   10 Nov  2 16:07 slsummer.txt -> summer.txt
1063921 -rw-r--r-- 1 student student    0 Nov  2 15:56 summer.txt
1063817 -rw-r--r-- 2 student student    0 Nov  2 15:56 winter.txt

#display the inode numbers of there three files, the hard links should have the same inode

student@vaiolabs:~$ ls -li winter.txt hlwinter.txt summer.txt
1063817 -rw-r--r-- 2 student student 0 Nov  2 15:56 hlwinter.txt
1063921 -rw-r--r-- 1 student student 0 Nov  2 15:56 summer.txt
1063817 -rw-r--r-- 2 student student 0 Nov  2 15:56 winter.txt

#use the find command to list the two hardlinked files
student@vaiolabs:~$ find . -inum 1063817
./hlwinter.txt
./winter.txt


#everything about a file is in the inode, except two things - name them!
#name of the file and the content of the file.

#create a symbolic link to summer.txt called slsummer.text

student@vaiolabs:~$ ln -s summer.txt slsummer.txt
student@vaiolabs:~$ ls
Desktop  Downloads  homework  Projects  slsummer.txt  summer.txt  winter.txt

#find all files with inode number 2. what does this information tell you?

#look at the directories /etc/init.d/ /etc/rc3.d/ do you see the links?

student@vaiolabs:~$ ls -l /etc/rc2.d
total 0
lrwxrwxrwx 1 root root 27 Mar 13  2020 K01speech-dispatcher -> ../init.d/speech-dispatcher
lrwxrwxrwx 1 root root 26 Mar 13  2020 S01console-setup.sh -> ../init.d/console-setup.sh
lrwxrwxrwx 1 root root 23 Mar 13  2020 S02lvm2-lvmpolld -> ../init.d/lvm2-lvmpolld
lrwxrwxrwx 1 root root 20 Mar 13  2020 S02mintsystem -> ../init.d/mintsystem
lrwxrwxrwx 1 root root 17 Mar 13  2020 S02rsyslog -> ../init.d/rsyslog
lrwxrwxrwx 1 root root 14 Mar 13  2020 S02sudo -> ../init.d/sudo
lrwxrwxrwx 1 root root 20 Mar 13  2020 S02sysfsutils -> ../init.d/sysfsutils
lrwxrwxrwx 1 root root 15 Mar 13  2020 S02uuidd -> ../init.d/uuidd
lrwxrwxrwx 1 root root 15 Mar 13  2020 S03acpid -> ../init.d/acpid
lrwxrwxrwx 1 root root 22 Mar 13  2020 S03acpi-support -> ../init.d/acpi-support
lrwxrwxrwx 1 root root 17 Mar 13  2020 S03anacron -> ../init.d/anacron
lrwxrwxrwx 1 root root 14 Mar 13  2020 S03cron -> ../init.d/cron
lrwxrwxrwx 1 root root 14 Mar 13  2020 S03dbus -> ../init.d/dbus
lrwxrwxrwx 1 root root 17 Mar 13  2020 S03hddtemp -> ../init.d/hddtemp
lrwxrwxrwx 1 root root 20 Mar 13  2020 S03irqbalance -> ../init.d/irqbalance
lrwxrwxrwx 1 root root 15 Mar 13  2020 S03rsync -> ../init.d/rsync
lrwxrwxrwx 1 root root 22 Mar 13  2020 S04avahi-daemon -> ../init.d/avahi-daemon
lrwxrwxrwx 1 root root 19 Mar 13  2020 S04bluetooth -> ../init.d/bluetooth
lrwxrwxrwx 1 root root 17 Mar 13  2020 S04lightdm -> ../init.d/lightdm
lrwxrwxrwx 1 root root 25 Mar 13  2020 S04network-manager -> ../init.d/network-manager
lrwxrwxrwx 1 root root 17 Mar 13  2020 S05openvpn -> ../init.d/openvpn
lrwxrwxrwx 1 root root 14 Mar 13  2020 S06cups -> ../init.d/cups
lrwxrwxrwx 1 root root 22 Mar 13  2020 S06cups-browsed -> ../init.d/cups-browsed
lrwxrwxrwx 1 root root 15 Mar 13  2020 S06saned -> ../init.d/saned
lrwxrwxrwx 1 root root 18 Mar 13  2020 S07plymouth -> ../init.d/plymouth


#look in /lib with ls -l

student@vaiolabs:~$ ls -l /lib
total 392
drwxr-xr-x  2 root root   4096 Mar 13  2020 apparmor
drwxr-xr-x  2 root root   4096 Mar 13  2020 brltty
drwxr-xr-x  2 root root   4096 Mar 13  2020 console-setup
lrwxrwxrwx  1 root root     21 Mar 13  2020 cpp -> /etc/alternatives/cpp
drwxr-xr-x  3 root root   4096 Mar 13  2020 crda
drwxr-xr-x  4 root root   4096 Mar 13  2020 cryptsetup
drwxr-xr-x 51 root root  12288 Sep  7 13:01 firmware
drwxr-xr-x  2 root root   4096 Mar 13  2020 hdparm
drwxr-xr-x  2 root root   4096 Mar 13  2020 ifupdown
drwxr-xr-x  2 root root   4096 Sep  7 13:02 init
-rwxr-xr-x  1 root root  74688 Feb  1  2019 klibc-ae-2A4n9ZnfImcw9WIt7dF-3OvQ.so
-rw-r--r--  1 root root 211312 Mar 12  2017 libdmraid.so.1.0.0.rc16
lrwxrwxrwx  1 root root     18 Feb 22  2019 libhandle.so.1 -> libhandle.so.1.0.3
-rw-r--r--  1 root root  14480 Feb 22  2019 libhandle.so.1.0.3
drwxr-xr-x  3 root root   4096 Mar 13  2020 lsb
drwxr-xr-x  2 root root   4096 Mar 13  2020 modprobe.d
drwxr-xr-x  3 root root   4096 Mar 13  2020 modules
drwxr-xr-x  2 root root   4096 Mar 13  2020 runit-helper
drwxr-xr-x  2 root root   4096 Mar 13  2020 security
drwxr-xr-x  8 root root   4096 Mar 13  2020 systemd
drwxr-xr-x 15 root root   4096 Nov  2  2019 terminfo
drwxr-xr-x  4 root root   4096 Mar 13  2020 udev
drwxr-xr-x  2 root root   4096 Mar 13  2020 ufw
drwxr-xr-x  4 root root  12288 Mar 13  2020 x86_64-linux-gnu


#use find command to look in your home directory for regular files that do not have one hard links
student@vaiolabs:~$ find ~ ! -links 1 -type f
/home/student/hlwinter.txt
/home/student/winter.txt
